FROM node:alpine3.15 AS development

WORKDIR /var/www/clients/app

COPY package*.json ./

RUN npm install --only=development

COPY . .

RUN npm run build

FROM node:alpine3.15 AS production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /var/www/clients/app

COPY package*.json ./

RUN npm install --only=production

COPY . .

COPY --from=development /var/www/clients/app/dist ./dist

CMD [ "node", "dist/main" ]